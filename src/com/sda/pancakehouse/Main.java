package com.sda.pancakehouse;

import java.util.Scanner;

import com.sda.pancakehouse.model.Pancake;
import com.sda.pancakehouse.model.PancakeHouse;

public class Main {
	public static void main(String[] args) {
		PancakeHouse house= new PancakeHouse();

		house.addPlate();
		house.addPlate();
		house.addPancakeToPlate(0);
		house.addPancakeToPlate(0);
		house.addPancakeToPlate(0);
//		house.addPancakeToPlate(1); //error

		house.addPancakeToPlate(1);
		house.addPancakeToPlate(1);
		house.addPancakeToPlate(1);
		
		house.addPancakeToPlate(1);
		house.addPancakeToPlate(0);
		house.addPancakeToPlate(1);
		
		house.printAllPancakes();
		

//		Scanner sc = new Scanner(System.in);
//		String inputLine = null;
//		while (sc.hasNextLine()) {
//			inputLine = sc.nextLine();
//			if (inputLine.equals("quit")) {
//				break;
//			}
//			parseLine(inputLine);
//		}
	}
}
