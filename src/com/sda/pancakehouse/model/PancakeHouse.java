package com.sda.pancakehouse.model;

import java.util.LinkedList;
import java.util.List;

public class PancakeHouse {
	private List<PancakePlate> plates;

	public PancakeHouse() {
		plates = new LinkedList<>();
	}

	public void addPlate() {
		PancakePlate plate = new PancakePlate();
		plates.add(plate);
		System.out.println("Dodano talerz " + plate.hashCode() + " o numerze: " + (plates.size() - 1));
	}

	public void addPancakeToPlate(int plateId) {
		if (plates.size() <= plateId) {
			System.err.println("Talerz nie istnieje.");
			return;
		}

		plates.get(plateId).addPancake(new Pancake());
	}

	public void getPancakeFromPlate(int plateId) {
		if (plates.size() <= plateId) {
			System.err.println("Talerz nie istnieje.");
			return;
		}

		plates.get(plateId).getPancake().eat();
	}

	public void printAllPancakes() {
		plates.forEach((plate) -> plate.printPancakes());
	}
}
