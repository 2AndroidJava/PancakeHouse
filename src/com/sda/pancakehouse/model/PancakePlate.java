package com.sda.pancakehouse.model;

import java.util.Stack;

public class PancakePlate {
	private Stack<Pancake> pancakes;

	public PancakePlate() {
		pancakes = new Stack<>();
	}

	public void addPancake(Pancake pancake) {
		System.out.println("Dodaje " + pancake.hashCode());
		pancakes.add(pancake);
	}

	public Pancake getPancake() {
		Pancake pancake = pancakes.pop();
		System.out.println("Usuwam " + pancake.hashCode() + " ze stolu " + hashCode());
		return pancake;
	}

	public void printPancakes() {
		System.out.println("Pancakes on plate " + hashCode() + " :");
		pancakes.forEach((pancake) -> System.out.println("       >" + pancake.hashCode() + "\n"));
	}
}
